// примеры соместимы с java 7

// для создания json использованы зависимости
groupId javax.json
artifactId javax.json-api
version 1.1.4

groupId org.glassfish
artifactId	javax.json
version 1.1.4

// для чтения json использовано
groupId com.google.code.gson
artifactId gson
version 2.2.2


// Пример получения токена авторизации

private static String getToken(){
	try {
		String serializedData =  Json.createObjectBuilder()
				.add("username", "MY_USERNAME") // используйте корректные значения учетной записи
				.add("password", "MY_PASSWORD")
				.build()
				.toString();

		URL url = new URL("https://online.sigmasms.ru/api/login");
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setDoOutput(true);
		OutputStream writer = urlConnection.getOutputStream();
		writer.write(serializedData.getBytes());
		writer.flush();
		writer.close();

		if(urlConnection.getResponseCode() == 200){
			InputStream inputStream = urlConnection.getInputStream();
			byte[] buffer = IOUtils.readFully(inputStream, -1, false);
			JsonElement jsonElement = new JsonParser().parse(new String(buffer));
			JsonElement token = jsonElement.getAsJsonObject().get("token");
			return token.getAsString();
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


// Пример использования

String token = getToken();
System.out.println("Token = " + token);




// Пример отсылки одного сообщения (через viber)

private static String sendOneMessage(String token) {
	try {
		String serializedData = Json.createObjectBuilder()
				.add("recipient", "+79000000000") // используйте корректный номер телефона
				.add("type", "viber")
				.add("payload", Json.createObjectBuilder()
						// убедитесь, что имя отправителя добавлено в ЛК в
						// разделе Компоненты ( https://online.sigmasms.ru/#/components )
						.add("sender", "X-City")
						.add("text", "Hello world!")
				)
				.build()
				.toString();

		URL url = new URL("https://online.sigmasms.ru/api/sendings");
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Authorization", token);

		urlConnection.setDoOutput(true);
		OutputStream writer = urlConnection.getOutputStream();
		writer.write(serializedData.getBytes());
		writer.flush();
		writer.close();

		if(urlConnection.getResponseCode() == 200) {
			InputStream inputStream = urlConnection.getInputStream();
			byte[] buffer = IOUtils.readFully(inputStream, -1, false);
			JsonElement jsonElement = new JsonParser().parse(new String(buffer));
			JsonElement messageId = jsonElement.getAsJsonObject().get("id");
			return messageId.getAsString();
		}

	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


// Пример использования
String token = getToken();
String messageId = sendOneMessage(token);
System.out.println("MessageId = " + messageId);




// Пример получения статуса отправленного сообшения

private static String getMessageStatus(String token, String messageId) {
	try {
		URL url = new URL("https://online.sigmasms.ru/api/sendings/" + messageId);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("Authorization", token);

		if(urlConnection.getResponseCode() == 200)
		{
			InputStream inputStream = urlConnection.getInputStream();
			byte[] buffer = IOUtils.readFully(inputStream, -1, false);
			JsonElement jsonElement = new JsonParser().parse(new String(buffer));
			JsonElement state = jsonElement.getAsJsonObject().get("state");
			JsonElement status = state.getAsJsonObject().get("status");
			return status.getAsString();
		}

	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


// Пример использования

String token = getToken();
String messageId = sendOneMessage(token);
String status = getMessageStatus(token, messageId);
System.out.println("Status = " + status);




// Пример загрузки изображения на сервер

private static String uploadImage(String token, String filePath) {
	try {
		File file = new File(filePath);
		FileInputStream fileStream = new FileInputStream(file);
		byte[] fileBytes = IOUtils.readFully(fileStream, -1, true);
		fileStream.close();

		URL url = new URL("https://online.sigmasms.ru/api/storage/");
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Authorization", token);
		urlConnection.setRequestProperty("Content-Type", "image/jpeg");

		urlConnection.setDoOutput(true);
		OutputStream writer = urlConnection.getOutputStream();

		writer.write(fileBytes);
		writer.flush();
		writer.close();

		if(urlConnection.getResponseCode() == 200){
			InputStream inputStream = urlConnection.getInputStream();
			byte[] buffer = IOUtils.readFully(inputStream, -1, false);
			JsonElement jsonElement = new JsonParser().parse(new String(buffer));
			JsonElement imageId = jsonElement.getAsJsonObject().get("key");
			return imageId.getAsString();
		}

	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


// Пример использования

String token = getToken();
String imageId = uploadImage(token, "./images/HelloWorld.jpg");
System.out.println("ImageId = " + imageId);




// Пример массоовой рассылки: сначала запланированная viber рассылка, затем пробуем смс-рассылку

private static String sendBulk(String token, String imageId) {
	try {
		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		dateFormat.setTimeZone(utcTimeZone);

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 5);
		Date after5Minutes = calendar.getTime();

		calendar.add(Calendar.MINUTE, 10);
		Date after15Minutes = calendar.getTime();

		String serializedData =  Json.createObjectBuilder()
				.add("recipient", Json.createArrayBuilder()
						.add("+79000000000")
						.add("+79000000001"))
				.add("type", "viber")
				.add("payload", Json.createObjectBuilder()
						.add("sender", "X-City")
						.add("text", "Hello world!" + dateFormat.format(after5Minutes))
						.add("image", imageId) // добавляем изображение
						.add("button", Json.createObjectBuilder() // добавляем кнопку
								.add("text", "best mailing service")
								.add("url", "https://sigmasms.ru/")
						)
				)
				.add("schedule", dateFormat.format(after5Minutes)) // выполнить через 5 минут
				.add("fallbacks", Json.createArrayBuilder()
						.add(Json.createObjectBuilder()
								.add("type", "sms")
								.add("payload", Json.createObjectBuilder()
										.add("sender", "B-Media")
										.add("text", "Hello world!" + dateFormat.format(after15Minutes))
								)
								.add("$options", Json.createObjectBuilder()
										.add("onTimeout", Json.createObjectBuilder()
												.add("timeout", 60 * 10) // через 10 минут
												.add("except", Json.createArrayBuilder()
														.add("seen")
														.add("delivered")
												)
										)
								)
						)
				)
				.build()
				.toString();

		URL url = new URL("https://online.sigmasms.ru/api/sendings");
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Authorization", token);

		urlConnection.setDoOutput(true);
		OutputStream writer = urlConnection.getOutputStream();
		writer.write(serializedData.getBytes());
		writer.flush();
		writer.close();

		if (urlConnection.getResponseCode() == 200){
			InputStream inputStream = urlConnection.getInputStream();
			byte[] buffer = IOUtils.readFully(inputStream, -1, false);
			JsonElement jsonElement = new JsonParser().parse(new String(buffer));
			JsonElement groupId = jsonElement.getAsJsonObject().get("groupId");
			return groupId.getAsString();
		}

	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
}


//  Пример использования

String token = getToken();
String imageId = uploadImage(token, "./images/HelloWorld.jpg");
String groupId = sendBulk(token, imageId);
System.out.println("GroupId = " + groupId);




// Проверка результатов рассылки по groupId. Результат будет выдан ТОЛЬКО после выполнения рассылки

private static void getCheckGroupInfo(String token, String groupId) {
	try {
		URL url = new URL("https://online.sigmasms.ru/api/sendings/?groupId=" + groupId);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("Authorization", token);

		if(urlConnection.getResponseCode() == 200) {
			InputStream inputStream = urlConnection.getInputStream();
			byte[] buffer = IOUtils.readFully(inputStream, -1, false);

			JsonElement jsonElement = new JsonParser().parse(new String(buffer));
			JsonArray dataArray = jsonElement.getAsJsonObject().get("data").getAsJsonArray();

			Map<String, Object> result = new HashMap<>();

			// собираем минимальную информацию по цепочкам сообщений
			for (int i = 0; i < dataArray.size(); i++) {
				JsonObject item = dataArray.get(i).getAsJsonObject();

				String chainId = item.get("chainId").getAsString();
				String type = item.get("type").getAsString();
				String status = item.get("state").getAsJsonObject().get("status").getAsString();
				if (!result.containsKey(chainId)) {
					result.put(chainId, new HashMap<String, String>());
				}
				HashMap<String, String> typeStatus = (HashMap<String, String>) result.get(chainId);
				typeStatus.put(type, status);
			}

			String resultString = Json.createObjectBuilder(result).build().toString();
			System.out.println("messages info:\n" + resultString);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
}

// Пример использования

String token = getToken();
String imageId = uploadImage(token, "./images/HelloWorld.jpg");
String groupId = sendBulk(token, imageId);
String info = getCheckGroupInfo(token, groupId);
System.out.println("messages info:\n" + info);
