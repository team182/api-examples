<?php ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
//
require_once(__DIR__.'/sigma.online.php');
//
define('SIGMA_API_TOKEN', 'yout_token'); // Auth Token from https://online.sigmasms.ru/#/account?currentTab=tokens
$sigma = new Sigmamsg(SIGMA_API_TOKEN);

/* Тесты  */
// tested 26.12.2024 on PHP 8.2
$phone = '+79998887766';

echo 'Тест СМС: <br>'.PHP_EOL;
$params = array(
    "type"       => "sms",
    "recipient"  => $phone,
    "payload"    => array(
        "sender" => "B-Media",
        "text"   => "Тестовое сообщение СМС"
    )
);
var_dump($sigma->send_msg($params));

echo PHP_EOL.'<br>Проверка статуса сообщения: <br>'.PHP_EOL;
var_dump($sigma->check_status('8a5e45f0-561a-4f3d-acd3-a37be5d09219'));

echo PHP_EOL.'<br>Загрузка картинки: <br>'.PHP_EOL;
$upload_image = $sigma->send_file(__DIR__.'/test.jpg');
var_dump($upload_image);

echo PHP_EOL.'<br>Тест Viber: <br>'.PHP_EOL;
//$upload_image = ['id' => 'f79c99d4-2b0d-44e3-a04f-67264e55bbda'];
if (isset($upload_image['id']) && !empty($upload_image['id'])) {
    $params = array(
        "type"       => "viber",
        "recipient"  => $phone,
        "payload"    => array(
            "sender" => "MediaGorod",
            "text"   => 'Тест сообщения Viber',
            "image"  => (isset($upload_image['id']) && !empty($upload_image['id'])) ? $upload_image['id'] : null,
            "button" => array(
                "text" => "Текст кнопки",
                "url"  => 'https://google.ru',
            )
        )
    );
    var_dump($sigma->send_msg($params));
}

echo PHP_EOL.'<br>Каскадная переотправка VK->Viber->SMS: <br>'.PHP_EOL;
$cascadeData = array(
    "type"       => 'vk',
    "recipient"  => $sigma->clear_phone($phone),
    "payload"    => array(
        "sender" => 'sigmamessaging',
        "text"   => 'Тест сообщения ВК',
    ),
    "fallbacks"  => [
        array(
            "type"       => 'viber',
            "payload"    => array(
                "sender" => 'MediaGorod',
                "text"   => 'Тест сообщения Viber',
                "image"  => (isset($upload_image['id']) && !empty($upload_image['id'])) ? $upload_image['id'] : null,
                "button" => array(
                    "text" => "Текст кнопки",
                    "url"  => 'https://google.ru',
                ),
            ),
            '$options' => array(
                "onStatus" => ["failed"],
                "onTimeout" => array(
                    "timeout" => 120,
                    "except"  => ["delivered", "seen"]
                )
            )
        ),
        array(
            "type"    => "sms",
            "payload" => array(
                "sender" => "B-Media",
                "text"   => 'Тест сообщения СМС'
            ),
            '$options' => array(
                "onStatus" => ["failed"],
                "onTimeout" => array(
                    "timeout" => 120,
                    "except"  => ["delivered", "seen"]
                )
            )
        )
    ]
);
var_dump($sigma->send_msg($cascadeData));