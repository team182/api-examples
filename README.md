# Примеры работы с API

* [1С](https://bitbucket.org/team182/api-examples/src/master/1С/)
* [С#](https://bitbucket.org/team182/api-examples/src/master/C%23/)
* [Java](https://bitbucket.org/team182/api-examples/src/master/Java/)
* [JavaScript](https://bitbucket.org/team182/api-examples/src/master/JavaScript/)
* [PHP](https://bitbucket.org/team182/api-examples/src/master/PHP/)
* [Python3](https://bitbucket.org/team182/api-examples/src/master/Python3/)