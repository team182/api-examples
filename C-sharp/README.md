**Enum с состояниями отправки сообщения:**

```csharp
/// <summary> Состояния отправки сообщений</summary>
public enum MessageStatus
{
    /// <summary> ожидает отправки </summary>
    pending,
    /// <summary> приостановлено </summary>
    paused,
    /// <summary> в обработке </summary>
    processing,
    /// <summary> отправлено </summary>
    sent,
    /// <summary> доставлено </summary>
    delivered,
    /// <summary> просмотрено </summary>
    seen,
    /// <summary> ошибка при обработке/отправке сообщения </summary>
    failed,

    False
}
```

**Пример функции для получение токена авторизации:**

```csharp
private async Task<string> GetToken()
{
    HttpClient client = null;
    try
    {
        var content = JsonConvert.SerializeObject(new
        {
            username = "username",
            password = "password"
        });
        var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

        var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/login");
        request.Content = stringContent;

        client = new HttpClient();
        var response = await client.SendAsync(request);
        var responseContent = await response.Content.ReadAsStringAsync();
        if (response.StatusCode == System.Net.HttpStatusCode.OK)
        {
            dynamic result = JsonConvert.DeserializeObject(responseContent);
            return (string) result.token;
        }
        else
        {
            return null;
        }
    }
    catch (Exception e)
    {
        // log
    }
    return null;
}
```

**Функция проверки токена авторизации на "свежесть", чтобы запросить новый:**

```csharp
private bool IsTokenIExpired(string token)
{
    var jwt = new JwtSecurityToken(token);
    return jwt.ValidTo < DateTime.UtcNow;
}
```
**Пример функции отправки sms 1 контакту:**

```csharp
private async Task<Guid?> SendSms(string token)
{
    var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/sendings");
    request.Headers.Add("Authorization", token);
    var content = JsonConvert.SerializeObject(new
    {
        recipient = "+79999999999",
        type = "sms",
        payload = new
        {
            // убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
            sender = "B-Media",
            text = "Hello world!"
        }
    });
    request.Content = new StringContent(content, Encoding.UTF8, "application/json");

    HttpClient client = null;
    try
    {
        client = new HttpClient();
        var response = await client.SendAsync(request);
        var resonseContent = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeAnonymousType(resonseContent, new
        {   // минимальный набор параметров для получения ответа
            Id = default(Guid?),
            Recipient = default(string),
            Status = default(string),
            Error = default(string)
        });

        if (response.StatusCode == System.Net.HttpStatusCode.OK && 
            result.Id != null)
        {
            return result.Id;
        }
        else
        {
            // log
        }
    }
    catch (Exception e)
    {
        // log
    }
    finally
    {
        client?.Dispose();
    }
    return null;
}
```

**Пример проверки статуса сообщения по его Id:**

```csharp
private async Task<MessageStatus> GetStatus(string token, Guid messageId)
{
    var url = new Uri("https://online.sigmasms.ru/api/sendings/");
    var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url, messageId.ToString()));
    request.Headers.Add("Authorization", token);

    HttpClient client = null;
    try
    {
        client = new HttpClient();
        var response = await client.SendAsync(request);
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeAnonymousType(content, new
        {
            Id = default(Guid?),
            ChainId = default(Guid?),
            State = new
            {
                Status = default(MessageStatus),
                Error = default(string)
            },
            // error
            Error = default(string),
            Message = default(string)
        });

        if (response.StatusCode == System.Net.HttpStatusCode.OK &&
            result.Id != null)
        {
            return result.State?.Status ?? MessageStatus.False;
        }
        else
        {
            // log
        }
    }
    catch (Exception e)
    {
        // log
    }
    finally
    {
        client?.Dispose();
    }
    return MessageStatus.False;
}
```

**Пример прямой загрузки картинки в файловое хранилище:**

```csharp
private async Task<Guid?> SendFile(string token)
{
    var filePath = "C:\\PathToFolder\\123.jpg";
    HttpClient client = null;
    try
    {
        var fileBytes = File.ReadAllBytes(filePath);

        var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/storage");
        request.Headers.Add("Authorization", token);
        request.Content = new ByteArrayContent(fileBytes);
        request.Content.Headers.Add("Content-Type", "image/jpeg");

        client = new HttpClient();
        var response = await client.SendAsync(request);
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeAnonymousType(content, new
        {
            Key = default(Guid?),
            // error
            Error = default(string),
            Message = default(string)
        });

        if (response.StatusCode == System.Net.HttpStatusCode.OK &&
            result.Key!= null)
        {
            return result.Key;
        }
        else
        {
            // log
        }
    }
    catch (Exception e)
    {
        // log
    }
    finally
    {
        client?.Dispose();
    }
    return null;
}
```

**Пример bulk-отправки viber-сообщения c расписанием и переотправкой:**

```csharp
private async Task<Guid?> SendViberBulk(string token, Guid imageId)
{
    var request = new HttpRequestMessage(HttpMethod.Post, "https://online.sigmasms.ru/api/sendings");
    request.Headers.Add("Authorization", token);
    var content = JsonConvert.SerializeObject(new
    {
        recipient = new[] { "+79999999999", "+79999999998" },
        type = "viber",
        payload = new
        {
            // убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
            sender = "X-City",

            text = "Hello world!" + DateTime.UtcNow.ToString("o"), // для уникальности сообщения в примере
            image = imageId.ToString(),
            button = new
            {
                text = "best sms service",
                url = "https://sigmasms.ru/"
            }
        },
        schedule = DateTime.UtcNow.AddMinutes(5).ToString("o"),
        fallbacks = new[]
        {
            new Dictionary<string, object>
            {
                { "type", "viber" },
                { "payload", new
                    {
                        // убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
                        sender = "X-City",

                        text = "Hello world!" + DateTime.UtcNow.AddMinutes(6).ToString("o"),
                        image = imageId.ToString(),
                        button = new
                        {
                            text = "best sms service",
                            url = "https://sigmasms.ru/"
                        }
                    }
                },
                { "$options", new
                    {
                        onTimeout = new
                        {
                            timeout = 60,
                            except = new[] { "seen" }
                        }
                    }
                }
            }
        }
    });
    request.Content = new StringContent(content, Encoding.UTF8, "application/json");

    HttpClient client = null;
    try
    {
        client = new HttpClient();
        var response = await client.SendAsync(request);
        var resonseContent = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeAnonymousType(resonseContent, new
        {   // минимальный набор параметров для получения ответа
            groupId = default(Guid?),
            Error = default(string)
        });

        if (response.StatusCode == System.Net.HttpStatusCode.OK &&
            result.groupId != null)
        {
            return result.groupId;
        }
        else
        {
            // log
        }
    }
    catch (Exception e)
    {
        // log
    }
    finally
    {
        client?.Dispose();
    }
    return null;
}
```

**Пример получения статусов сообщений по groupId:**

```csharp
private async Task<bool> GetGroupStatus(string token, Guid groupId)
{
    var builder = new UriBuilder("https://online.sigmasms.ru/api/sendings/");

    var parameters = HttpUtility.ParseQueryString("");
    parameters.Add("groupId", groupId.ToString());
    builder.Query = parameters.ToString();
    var request = new HttpRequestMessage(HttpMethod.Get, builder.Uri);
    request.Headers.Add("Authorization", token);

    HttpClient client = null;
    try
    {
        client = new HttpClient();
        var response = await client.SendAsync(request);
        var content = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeAnonymousType(content, new
        {
            Offset = default(int),
            Limit = default(int),
            Data = new[] {
                new
                {
                    Id = default(Guid?),
                    ChainId = default(Guid?),
                    GroupId = default(Guid?),
                    State = new
                    {
                        Status = default(MessageStatus),
                        Error = default(string)
                    }
                }
            },
            // error
            Error = default(string),
            Message = default(string)
        });

        if (response.StatusCode == System.Net.HttpStatusCode.OK && 
            string.IsNullOrEmpty(result.Error) &&
            result.Data != null &&
            result.Data.Any(i => string.IsNullOrEmpty(i.State.Error)))
        {
            var successMessages = result.Data
                .Where(i =>
                    i.State.Status == MessageStatus.delivered ||
                    i.State.Status == MessageStatus.sent ||
                    i.State.Status == MessageStatus.seen)
                .Select(i => i.Id)
                .ToList();

            return true;
        }
        else
        {
            // log
        }
    }
    catch (Exception e)
    {
        // log
    }
    finally
    {
        client?.Dispose();
    }
    return false;
}
```