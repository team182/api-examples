import requests
import datetime
import json
from functools import reduce

def get_token():
    '''
    Получение токена авторизации
    '''
    try:
        response = requests.post(
            'https://online.sigmasms.ru/api/login',
            json = dict(
                username = 'your_login',
                password = 'yout_password'
            )
        )

        if response and response.status_code == 200:
            return response.json().get('token')

    except Exception as e:
        raise e

def upload_image(token, image_path):
    '''
    Отправка изображения, для дальнейшей отправки через мессенджер
    :param token: токен авторизации
    :param image_path: путь к изображению
    :return: id изображения
    '''
    try:
        with open(image_path, 'rb') as file_content:
            file_as_bytes = file_content.read()
        if not file_as_bytes:
            return None

        response = requests.post(
            'https://online.sigmasms.ru/api/storage/',
            headers = {'Authorization': token, 'Content-Type': 'image/jpeg'},
            data = file_as_bytes)

        if response and response.status_code == 200:
            return response.json().get('key')

    except Exception as e:
        raise e

def send_bulk(token, image_id):
    '''
    Делает bulk-отправку на viber, через 10мин пробует отослать еще раз, но через sms
    :param token: токен авторизации
    :param image_id: id изображения
    :return: id группы, по которому можно получить статус рассылки. Информация о рассылке будет доступна только после выполнения задачи рассылки.
    '''
    try:
        response = requests.post(
            'https://online.sigmasms.ru/api/sendings',
            headers = {'Authorization': token, 'Content-Type': 'application/json'},
            json = dict(
                recipient = ['+70000000000', '+70000000001'],
                type = 'viber',
                payload = dict(
                    # убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
                    sender = 'X-City',
                     # для уникальности сообщения добаляем время
                    text = 'Hello world!' + datetime.datetime.utcnow().isoformat(),
                    image = image_id,
                    button = dict(
                        text = 'best mailing service',
                        url = 'https://sigmasms.ru/'
                    )
                ),
                # через 10 минут пробуем еще раз
                schedule = (datetime.datetime.utcnow() + datetime.timedelta(minutes = 10)).isoformat(),
                fallbacks = [{
                    'type': 'sms',
                    'payload': dict(
                        # убедитесь, что имя отправителя добавлено в ЛК в разделе Компоненты ( https://online.sigmasms.ru/#/components )
                        sender = 'B-Media',
                        text = 'Hello world!' + (datetime.datetime.utcnow() + datetime.timedelta(minutes = 10)).isoformat()
                    ),
                    '$options': dict(
                        onTimeout = {
                            'timeout': 600,
                            'except': ['seen', 'delivered']
                        }
                    )
                }]
            )
        )

        if response and response.status_code == 200:
            return response.json().get('groupId')

    except Exception as e:
        raise e

def check_bulk_status(token, group_id):
    """
    Проверка статуса отправлений по groupId
    ВАЖНО: если задача рассылки еще не исполненена, то результат будет пустой
    """
    try:
        response = requests.get(
            'https://online.sigmasms.ru/api/sendings/?groupId=' + group_id,
            headers = {'Authorization': token, 'Content-Type': 'application/json'})

        if not response or response.status_code != 200:
            return

        data = response.json().get('data')
        if not data:
            return

        data.sort(key=lambda el:
            datetime.datetime.strptime(el.get('createdAt'), "%Y-%m-%dT%H:%M:%S.%fZ"));

        result = reduce(reducer_func, data, {})
        print(json.dumps(result, indent=4))

    except Exception as e:
        raise e

def reducer_func(group, element):
    '''
    вспомогательная функция для группировки данных по chainId
    '''
    chain_id = element.get('chainId')
    chainElement = group.get(chain_id, [])
    chainElement.append({
        'type': element.get('type'),
        'status': element.get('state', {}).get('status'),
        'createdAt': element.get('createdAt')
    })
    group[chain_id] = chainElement
    return group


token = get_token()
print('token=' + token)

image_id = upload_image(token, '../helloworld.jpg')
group_id = send_bulk(token, image_id)
check_bulk_status(token, group_id)
